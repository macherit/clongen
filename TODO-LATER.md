### NOW

- Fix painting nodes not node (icons)
- Guardar colores
- Deshabilitar botones cuando se exporta imagen o xls
- Agrandar svg en fullscreen

### LATER

- Revisar construcción del gráfico para trackear cambios sobre nodos especificos y actualizar esa información en vez de repintar todo
- Fix problem center
- Fix problem fit
- Review save image after fullscreen

