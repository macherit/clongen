const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const compression = require('compression');
const serveStatic = require('serve-static');
const crypto = require('crypto');
const fs = require('fs');

require('./models/db');
const fileUpload = require('express-fileupload');

const guardian = require('./config/guardian');
const passport = require('passport');

const users = require('./routes/userRoutes');
require('./config/passport');

const app = express();
app.use(compression());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//create a cors middleware
app.use(function(req, res, next) {
  //set headers to allow cross origin request.
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(cookieParser());

app.use(fileUpload());

/*
 * IMAGES
 */

app.post('/api/upload', guardian({}), function(req, res) {
  if (!req.files) return res.status(400).send('No files were uploaded.');

  const output = [];

  if (!Array.isArray(req.files.images)) req.files.images = [req.files.images];

  for (let i = 0; i < req.files.images.length; i++) {
    let imagen = req.files.images[i];

    if (new RegExp(/^(image\/png|image\/jpg|image\/jpeg|image\/gif)$/).test(imagen.mimetype)) {
      let ubicacion = process.cwd();

      if (process.env.NODE_ENV === 'production')
        fs.existsSync(ubicacion + '/public/images/uploads/') ||
          fs.mkdirSync(ubicacion + '/public/images/uploads/');

      let imgName =
        '/public/images/uploads/' +
        crypto.randomBytes(5).toString('hex') +
        imagen.name
          .toLowerCase()
          .replace(/\s/g, '-')
          .replace(/á/g, 'a')
          .replace(/é/g, 'e')
          .replace(/í/g, 'i')
          .replace(/ó/g, 'o')
          .replace(/ú/g, 'u')
          .replace(/ñ/g, 'n')
          .replace(/[^a-zA-Z0-9\.-_]/g, '');

      imagen.mv(ubicacion + imgName, function(err) {
        if (err) {
          console.log(err);
          return res.status(500).send(err);
        }

        output.push({
          message: 'File uploaded!',
          uri: imgName,
        });

        if (i == req.files.images.length - 1) res.json(output);
      });
    } else {
      return res.status(500).send({ message: 'Error' });
    }
  }
});

app.post('/api/upload-videos', guardian({}), function(req, res) {
  if (!req.files) return res.status(400).send('No files were uploaded.');

  const output = [];

  if (!Array.isArray(req.files.videos)) req.files.videos = [req.files.videos];

  for (let i = 0; i < req.files.videos.length; i++) {
    let video = req.files.videos[i];

    if (video.mimetype === 'video/mp4' || video.mimetype === 'video/x-m4v') {
      let ubicacion = process.cwd();

      if (process.env.NODE_ENV === 'production')
        fs.existsSync(ubicacion + '/public/videos/uploads/') ||
          fs.mkdirSync(ubicacion + '/public/videos/uploads/');

      let videoName =
        '/public/videos/uploads/' +
        crypto.randomBytes(5).toString('hex') +
        video.name
          .toLowerCase()
          .replace(/\s/g, '-')
          .replace(/á/g, 'a')
          .replace(/é/g, 'e')
          .replace(/í/g, 'i')
          .replace(/ó/g, 'o')
          .replace(/ú/g, 'u')
          .replace(/ñ/g, 'n')
          .replace(/[^a-zA-Z0-9\.-_]/g, '');

      video.mv(ubicacion + videoName, function(err) {
        if (err) {
          console.log(err);
          return res.status(500).send(err);
        }

        output.push({
          message: 'File uploaded!',
          uri: videoName,
        });

        if (i == req.files.videos.length - 1) res.json(output);
      });
    } else {
      return res.status(500).send({ message: 'Error' });
    }
  }
});

app.post('/api/upload-file', guardian({}), function(req, res) {
  if (!req.files) return res.status(400).send('No files were uploaded.');

  const output = [];

  if (!Array.isArray(req.files.files)) req.files.files = [req.files.files];

  for (let i = 0; i < req.files.files.length; i++) {
    let file = req.files.files[i];

    if (
      new RegExp(
        /^(application\/msword|application\/vnd.openxmlformats-officedocument.wordprocessingml.document|application\/vnd.ms-excel|application\/vnd.openxmlformats-officedocument.spreadsheetml.sheet|application\/vnd.ms-powerpoint|application\/vnd.openxmlformats-officedocument.presentationml.presentation|text\/csv|application\/vnd.oasis.opendocument.text|application\/pdf|text\/plain|application\/vnd.oasis.opendocument.presentation)$/,
      ).test(file.mimetype)
    ) {
      let ubicacion = process.cwd();

      if (process.env.NODE_ENV === 'production')
        fs.existsSync(ubicacion + '/public/files/uploads/') ||
          fs.mkdirSync(ubicacion + '/public/files/uploads/');

      let fileName =
        '/public/files/uploads/' +
        crypto.randomBytes(5).toString('hex') +
        file.name
          .toLowerCase()
          .replace(/\s/g, '-')
          .replace(/á/g, 'a')
          .replace(/é/g, 'e')
          .replace(/í/g, 'i')
          .replace(/ó/g, 'o')
          .replace(/ú/g, 'u')
          .replace(/ñ/g, 'n')
          .replace(/[^a-zA-Z0-9\.-_]/g, '');

      file.mv(ubicacion + fileName, function(err) {
        if (err) {
          console.log(err);
          return res.status(500).send(err);
        }

        output.push({
          message: 'File uploaded!',
          uri: fileName,
        });

        if (i == req.files.files.length - 1) res.json(output);
      });
    } else {
      return res.status(500).send({ message: 'Error' });
    }
  }
});

app.use(passport.initialize());

app.use('/api/users', users);

const svgconverter = require('./routes/svgconverterRoutes');
app.use('/api/svgconverter', svgconverter);

const problem = require('./routes/problemRoutes');
app.use('/api/problems', problem);

if (process.env.NODE_ENV !== 'production') {
  // Endpoint para generar random hashes que sirvan como identificadores de ROLES

  app.get('/app/get_hash', (req, res) => {
    res.send(crypto.randomBytes(5).toString('hex'));
  });

  // PUBLIC

  app.use('/public', serveStatic(path.join(__dirname, '/public/')));

  // SIRVE LIBRERIAS BASICAS

  app.use('/node_modules/', serveStatic(path.join(__dirname, 'node_modules/')));

  // SIRVE SITIO PRINCIPAL

  // app.use('/', serveStatic(path.join(__dirname, 'static/site')));

  // SIRVE ADMIN

  // app.use('/admin', serveStatic(path.join(__dirname, 'static/admin')));
}

////////MAILING

const mailing = require('./modules/mailing');
app.use('/api/mailing', mailing);

//////

// process.env.NODE_ENV !== "production" &&
//   app.get("/*", function(req, res) {
//     if (req.xhr) {
//       return res.status(404).send(req.url + " not found");
//     }
//     res.sendfile(path.resolve(__dirname + "/static/site/index.html"));
//   });

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
