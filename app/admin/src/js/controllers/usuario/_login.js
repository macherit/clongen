angular
  .module("UsuarioModule")
  .controller("user.login", ($scope, $window, auth) => {
    $scope.email = "";
    $scope.password = "";
    $scope.failLogin = false;

    $scope.sent = false;

    $scope.sendForm = () => {
      $scope.sent = true;
      auth
        .login({ email: $scope.email, password: $scope.password })
        .then(status => {
          if (status === 200) {
            $window.location.href = "#/";
            $window.location.reload();
          } else if (status === 401) {
            $scope.failLogin = true;
          }
          $scope.sent = false;
        });
    };
  });
