angular
  .module("UsuarioModule")
  .controller("user.logout", ($scope, $window, auth) => {
    auth.logout();
    $window.location.href = "#/";
    $window.location.reload();
  });
