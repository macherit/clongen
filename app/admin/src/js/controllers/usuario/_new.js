// angular
//   .module("UsuarioModule")
//   .controller("user.new", ($scope, $window, auth, $http) => {
//     $scope.nuevoUsuario = {
//       email: "",
//       nombre: "",
//       apellido: ""
//     };
//
//     $scope.registrarUsuario = () => {
//       $scope.f = document.getElementById("user-avatar").files[0];
//
//       $scope.notMatch = false;
//       $scope.dupped = false;
//
//       if ($scope.passwordCheck === $scope.nuevoUsuario.password) {
//         $http({
//           method: "POST",
//           url: `/api/users/`,
//           headers: auth.headers,
//           data: $scope.nuevoUsuario
//         })
//           .then(response => {
//             if (auth.user) {
//               if ($scope.f) {
//                 let data = new FormData();
//                 data.append("imagenes", $scope.f);
//
//                 $http
//                   .post("/upload", data, {
//                     headers: {
//                       transformRequest: angular.identity,
//                       "Content-Type": undefined,
//                       Authorization: "Bearer " + auth.token
//                     }
//                   })
//                   .then(response => {
//                     if (response.status === 200) {
//                       $scope.nuevoUsuario.avatar = response.data[0].uri;
//
//                       $http({
//                         method: "PUT",
//                         url: `/api/users/${auth.user._id}`,
//                         headers: auth.headers,
//                         data: $scope.nuevoUsuario
//                       })
//                         .then(response => {
//                           $window.location.href = "#/users/perfil";
//                           $window.location.reload();
//                         })
//                         .catch(err => {
//                           console.log(err);
//                         });
//                     }
//                   });
//               }
//             } else {
//               $window.location.href = "#/users/perfil";
//               $window.location.reload();
//             }
//           })
//           .catch(err => {
//             console.log(err);
//             if (err.data.error.code)
//               $scope.dupped = err.data.error.code == 11000 ? true : false;
//           });
//       } else {
//         $scope.notMatch = true;
//       }
//     };
//   });
