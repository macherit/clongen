angular
  .module("UsuarioModule")
  .controller("user.show", ($scope, auth, $http) => {
    $scope.user = {};

    $http({
      method: "GET",
      url: `/api/users/${auth.user._id}`,
      headers: auth.headers
    })
      .then(({data}) => {
        $scope.user = data;
      }, error => {
        console.error(error);
      });
  });
