angular
  .module("AdminApp", ["ngRoute", "UsuarioModule"])
  .controller("mainController", ($scope, $location, $rootScope, auth) => {
    $rootScope.user = auth.user;

    $rootScope.er = i => {
      let r = false;
      if (auth.user) {
        let rs = [];
        r = rs[i] ? auth.evalRole(rs[i]) : false;
      }
      return r;
    };

    $scope.rutas = [];

    $rootScope.$on("$routeChangeSuccess", () => {
      $scope.rutas = [];

      let rutas = $location.path().split("/");

      if (rutas[1] != "") {
        let path = "";

        let bar = "";

        for (let i = 0; i < rutas.length; i++) {
          if (rutas[i] != "") {
            let nombre = "";
            path += bar + rutas[i];
            nombre = rutas[i] = rutas[i].replace(/-/g, " ");
            $scope.rutas.push({ nombre: nombre, link: path, active: false });
            bar = "/";
          }
        }

        $scope.rutas[$scope.rutas.length - 1].active = true;
      }
    });

    $rootScope.deleteProp = (obj, prop) => {
      delete obj[prop];
    };
  });
