angular.module("AdminApp").config(($routeProvider, $locationProvider) => {
  $locationProvider.hashPrefix("");

  $routeProvider
    //OTHERWISE

    .otherwise({
      redirectTo: "/users/iniciar-sesion",
      templateUrl: "views/user/login.html"
    });

  // use the HTML5 History API
  // $locationProvider.html5Mode(true);
});
