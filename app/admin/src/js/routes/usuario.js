angular.module("AdminApp").config(($routeProvider, $locationProvider) => {
  $locationProvider.hashPrefix("");

  $routeProvider
    // USUARIO

    // .when("/users", {
    //   templateUrl: "views/user/index.html",
    //   controller: "user.index"
    // })
    // .when("/users/registrar", {
    //   templateUrl: "views/user/new.html",
    //   controller: "user.new"
    // })
    .when("/users/iniciar-sesion", {
      templateUrl: "views/user/login.html",
      controller: "user.login",
      resolve: {
        secureAccess: (auth, $window) => {
          if (auth.user) {
            $window.location.href = "#/";
          }
        }
      }
    })
    .when("/users/cerrar-sesion", {
      templateUrl: "views/home/index.html",
      resolve: {
        secureAccess: (auth, $window) => {
          if (!auth.user) {
            $window.location.href = "#/";
          }
        }
      },
      controller: "user.logout"
    })
    .when("/users/perfil", {
      templateUrl: "views/user/perfil.html",
      resolve: {
        secureAccess: (auth, $window) => {
          if (!auth.user) {
            $window.location.href = "#/";
          }
        }
      },
      controller: "user.show"
    })
    .when("/user/:id", {
      templateUrl: "views/user/show.html",
      controller: "user.show"
    });
});
