angular
  .module("MainApp")
  .config(($httpProvider, $routeProvider, $locationProvider) => {
    $httpProvider.defaults.headers.common["X-Requested-With"] =
      "XMLHttpRequest";

    $locationProvider.html5Mode(true);

    $routeProvider
      .when("/", {
        templateUrl: "/views/home/index.html",
        controller: "home.index"
      })

      .otherwise({
        templateUrl: "/views/home/index.html",
        redirectTo: "/"
      });
  });
