const jwt = require('express-jwt');
const userModel = require('../models/userModel.js');

module.exports = options => {
  return [
    jwt({ secret: process.env.BCF, userProperty: 'payload' }),
    (req, res, next) => {
      if (
        !req.payload ||
        !(options.mailToken
          ? req.payload.uid
          : options.activationToken
          ? req.payload.acuid
          : req.payload._id)
      ) {
        res.status(401).json({ message: 'Unauthorized' });
      } else {
        userModel
          .findOne({
            _id: options.mailToken
              ? req.payload.uid
              : options.activationToken
              ? req.payload.acuid
              : req.payload._id,
          })
          .exec((error, user) => {
            if (error) {
              return res.status(500).json({ message: 'Internal error' });
            } else {
              if (!user) {
                return res.status(401).json({
                  message: 'Unauthorized',
                });
              } else {
                if (!options.activationToken) {
                  console.log(user.activation);
                  if (!user.activation) {
                    if (options.mailToken) {
                      if (
                        !req.payload.exp ||
                        new Date(req.payload.exp) < new Date() ||
                        !user.recovery ||
                        user.recovery === '' ||
                        user.recovery !== req.headers.authorization.split('Bearer ')[1]
                      ) {
                        return res.status(401).json({
                          message: 'Unauthorized',
                        });
                      } else {
                        return next();
                      }
                    } else if (options.sameUser) {
                      if (user._id != req.payload._id) {
                        return res.status(401).json({
                          message: 'Unauthorized',
                        });
                      } else {
                        return next();
                      }
                    } else {
                      return next();
                    }
                  } else {
                    return res.status(409).json({
                      message: 'Requires activation',
                    });
                  }
                } else {
                  if (
                    !req.payload.exp ||
                    new Date(req.payload.exp) < new Date() ||
                    !user.activation ||
                    user.activation === '' ||
                    user.activation !== req.headers.authorization.split('Bearer ')[1]
                  ) {
                    return res.status(401).json({
                      message: 'Unauthorized',
                    });
                  } else {
                    next();
                  }
                }
              }
            }
          });
      }
    },
  ];
};
