const problemModel = require('../models/problemModel.js');

const cleanCausesIdeas = causes =>
  causes
    .filter(cause => cause.description)
    .map(cause => {
      cause.ideas = cause.ideas.filter(idea => idea.description);
      return cause;
    });
/**
 * problemController.js
 *
 * @description :: Server-side logic for managing problems.
 */

const findProblemById = (res, id, user_id, email, callback) => {
  problemModel
    .findOne({
      $and: [
        {
          _id: id,
        },
        {
          $or: [
            {
              owner: user_id,
            },
            {
              public: true,
            },
            {
              allowed: {
                $elemMatch: { email },
              },
            },
          ],
        },
      ],
    })
    .exec((err, problem) => {
      if (err) {
        return res.status(500).json({
          message: 'Error when getting problem.',
          error: err,
        });
      }
      if (!problem) {
        return res.status(404).json({
          message: 'No such problem',
        });
      }

      callback(problem);
    });
};

const saveAndReturn = (problem, res, status = 200) => {
  problem.save((err, problem) => {
    if (err) {
      console.log(err);
      return res.status(500).json({
        message: 'Error when creating problem',
        error: err,
      });
    }
    return res.status(status).json(problem);
  });
};

module.exports = {
  /**
   * problemController.list()
   */
  list(req, res) {
    problemModel.find((err, problems) => {
      if (err) {
        return res.status(500).json({
          message: 'Error when getting problem.',
          error: err,
        });
      }
      return res.json(problems);
    });
  },

  my(req, res) {
    problemModel
      .find({
        $or: [
          {
            owner: req.payload._id,
          },
          {
            allowed: {
              $elemMatch: { email: req.payload.email },
            },
          },
        ],
      })
      .populate('owner')
      .exec((err, problems) => {
        if (err) {
          return res.status(500).json({
            message: 'Error when getting problem.',
            error: err,
          });
        }
        return res.json(problems);
      });
  },

  /**
   * problemController.show()
   */

  is_public(req, res) {
    problemModel.findOne({ _id: req.params.id, public: true }).exec((error, problem) => {
      if (error || !problem) {
        return res.status(500).json({
          message: 'Error when getting problem.',
          error: error,
        });
      }
      return res.json({});
    });
  },

  get_public(req, res) {
    problemModel.findOne({ _id: req.params.id, public: true }).exec((error, problem) => {
      if (error || !problem) {
        return res.status(500).json({
          message: 'Error when getting problem.',
          error: error,
        });
      }
      return res.json(problem);
    });
  },

  show(req, res) {
    findProblemById(res, req.params.id, req.payload._id, req.payload.email, problem =>
      res.json(problem),
    );
  },

  /**
   * problemController.create()
   */
  create(req, res) {
    const problem = new problemModel({
      owner: req.payload._id,
      _id: req.body._id,
      description: req.body.description,
      actions: req.body.actions,
      weights: req.body.weights,
      emojis: req.body.emojis,
      images: req.body.images,
      comments: req.body.comments,
      links: req.body.links,
      createdAt: req.body.createdAt,
      updatedAt: req.body.updatedAt,
      votes: req.body.votes,
      videos: req.body.videos,
      files: req.body.files,
      pos: req.body.pos,
      causes: cleanCausesIdeas(req.body.causes),
    });

    saveAndReturn(problem, res, 201);
  },

  /**
   * problemController.update()
   */
  update(req, res) {
    findProblemById(res, req.params.id, req.payload._id, req.payload.email, problem => {
      problem.description = req.body.description ? req.body.description : problem.description;
      problem.weights = req.body.weights ? req.body.weights : problem.weights;
      problem.pos = req.body.pos ? req.body.pos : problem.pos;
      problem.emojis = req.body.emojis ? req.body.emojis : problem.emojis;
      problem.images = req.body.images ? req.body.images : problem.images;
      problem.comments = req.body.comments ? req.body.comments : problem.comments;
      problem.links = req.body.links ? req.body.links : problem.links;
      problem.updatedAt = new Date();
      problem.votes = req.body.votes ? req.body.votes : problem.votes;
      problem.videos = req.body.videos ? req.body.videos : problem.videos;
      problem.files = req.body.files ? req.body.files : problem.files;
      problem.causes = req.body.causes ? cleanCausesIdeas(req.body.causes) : problem.causes;

      saveAndReturn(problem, res);
    });
  },

  favourite(req, res) {
    findProblemById(res, req.params.id, req.payload._id, req.payload.email, problem => {
      let oldTrash = false;
      if (problem.actions[req.payload._id]) {
        oldTrash = problem.actions[req.payload._id].trash;
        delete problem.actions[req.payload._id];
      }
      problem.actions[req.payload._id] = {
        favourite: true,
        trash: oldTrash,
      };
      const newActions = problem.actions;
      problem.actions = null;
      problem.actions = newActions;
      problem.updatedAt = new Date();

      saveAndReturn(problem, res);
    });
  },

  unfavourite(req, res) {
    findProblemById(res, req.params.id, req.payload._id, req.payload.email, problem => {
      let oldTrash = false;
      if (problem.actions[req.payload._id]) {
        oldTrash = problem.actions[req.payload._id].trash;
        delete problem.actions[req.payload._id];
      }
      problem.actions[req.payload._id] = {
        favourite: false,
        trash: oldTrash,
      };

      const newActions = problem.actions;
      problem.actions = null;
      problem.actions = newActions;
      problem.updatedAt = new Date();

      saveAndReturn(problem, res);
    });
  },

  public(req, res) {
    findProblemById(res, req.params.id, req.payload._id, req.payload.email, problem => {
      problem.public = true;
      problem.updatedAt = new Date();

      saveAndReturn(problem, res);
    });
  },

  unpublic(req, res) {
    findProblemById(res, req.params.id, req.payload._id, req.payload.email, problem => {
      problem.public = false;
      problem.updatedAt = new Date();

      saveAndReturn(problem, res);
    });
  },

  trash(req, res) {
    findProblemById(res, req.params.id, req.payload._id, req.payload.email, problem => {
      let oldFavourite = false;
      if (problem.actions[req.payload._id]) {
        oldFavourite = problem.actions[req.payload._id].favourite;
        delete problem.actions[req.payload._id];
      }
      problem.actions[req.payload._id] = {
        trash: true,
        favourite: oldFavourite,
      };

      const newActions = problem.actions;
      problem.actions = null;
      problem.actions = newActions;
      problem.updatedAt = new Date();

      saveAndReturn(problem, res);
    });
  },

  untrash(req, res) {
    findProblemById(res, req.params.id, req.payload._id, req.payload.email, problem => {
      let oldFavourite = false;
      if (problem.actions[req.payload._id]) {
        oldFavourite = problem.actions[req.payload._id].favourite;
        delete problem.actions[req.payload._id];
      }
      problem.actions[req.payload._id] = {
        trash: false,
        favourite: oldFavourite,
      };

      const newActions = problem.actions;
      problem.actions = null;
      problem.actions = newActions;
      problem.updatedAt = new Date();

      saveAndReturn(problem, res);
    });
  },

  allow(req, res) {
    const id = req.params.id;
    if (req.body.email) {
      findProblemById(res, req.params.id, req.payload._id, req.payload.email, problem => {
        const allowedIndex = problem.allowed.findIndex(a => a.email === req.body.email);

        const allowed = {
          email: req.body.email,
          permission: req.body.permission ? req.body.permission : 'view',
        };

        if (allowedIndex >= 0) {
          problem.allowed.splice(allowedIndex, 1, allowed);
        } else {
          problem.allowed.push(allowed);
        }

        problem.updatedAt = new Date();

        saveAndReturn(problem, res);
      });
    }
  },
  disallow(req, res) {
    const id = req.params.id;
    if (req.body.email) {
      findProblemById(res, req.params.id, req.payload._id, req.payload.email, problem => {
        const allowedIndex = problem.allowed.findIndex(a => a.email === req.body.email);

        if (allowedIndex >= 0) {
          problem.allowed.splice(allowedIndex, 1);
        }

        problem.updatedAt = new Date();

        saveAndReturn(problem, res);
      });
    }
  },

  /**
   * problemController.remove()
   */
  remove(req, res) {
    const id = req.params.id;
    problemModel.findByIdAndRemove(id, (err, problem) => {
      if (err) {
        return res.status(500).json({
          message: 'Error when deleting the problem.',
          error: err,
        });
      }
      return res.status(204).json();
    });
  },
};
