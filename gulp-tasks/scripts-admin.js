const gulp = require("gulp");
const concat = require("gulp-concat");
const babel = require("gulp-babel");
const sourcemaps = require("gulp-sourcemaps");
const uglify = require("gulp-uglify");
const ngAnnotate = require("gulp-ng-annotate");
const refresh = require("gulp-refresh");

gulp.task("js_admin", () => {
  gulp
    .src(["app/admin/src/js/**/!(_)*.js", "app/admin/src/js/**/_*.js"])
    .pipe(sourcemaps.init())
    .pipe(babel({ presets: ["@babel/env"] }))
    .pipe(concat("static/admin/js/app.min.js"))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("."))
    .pipe(refresh());

  gulp
    .src(["app/admin/src/libs/**/*.js"])
    .pipe(sourcemaps.init())
    .pipe(babel({ presets: ["@babel/env"] }))
    .pipe(concat("static/admin/js/libs.min.js"))
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("."))
    .pipe(refresh());
});

gulp.task("js_watch_admin", ["js_admin"], () => {
  refresh.listen(35729);
  gulp.watch(
    [
      "app/admin/src/js/**/!(_)*.js",
      "app/admin/src/js/**/_*.js",
      "app/admin/src/libs/**/*.js"
    ],
    ["js_admin"]
  );
});

gulp.task("js_admin_d", () => {
  gulp
    .src(["app/admin/src/js/**/!(_)*.js", "app/admin/src/js/**/_*.js"])
    .pipe(sourcemaps.init())
    .pipe(babel({ presets: ["@babel/env"] }))
    .pipe(concat("dist/static/admin/js/app.min.js"))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("."));

  gulp
    .src(["app/admin/src/libs/**/*.js"])
    .pipe(sourcemaps.init())
    .pipe(babel({ presets: ["@babel/env"] }))
    .pipe(concat("dist/static/admin/js/libs.min.js"))
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("."));
});

gulp.task("js_dist_admin", ["js_admin_d"]);
