var gulp = require("gulp");

gulp.task("webmaster_dist", function() {
  return gulp
    .src(["sitemap.xml", "robots.txt", "google*.html"])
    .pipe(gulp.dest("dist/static/site/"));
});
