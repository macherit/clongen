const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const pos = {
  x: { type: Number, default: 0 },
  y: { type: Number, default: 0 },
  w: { type: Number, default: 0 },
  h: { type: Number, default: 0 },
};

const problemSchema = new Schema({
  _id: { type: String, required: true },
  owner: { type: Schema.Types.ObjectId, ref: 'user' },
  description: { type: String, required: true },
  public: { type: Boolean, default: false },
  actions: {},
  createdAt: { type: Date, required: true, default: new Date() },
  updatedAt: { type: Date, required: true, default: new Date() },
  weights: {
    cost: Number,
    time: Number,
    resources: Number,
  },
  emojis: [
    {
      url: { type: String, required: true },
      pos,
    },
  ],
  images: [
    {
      url: { type: String, required: true },
      pos,
    },
  ],
  comments: {
    list: [],
    pos,
  },
  links: {
    list: [],
    pos,
  },
  votes: {
    list: [],
    pos,
  },
  videos: {
    list: [
      {
        url: { type: String, required: true },
        name: { type: String, required: true },
        upload: { type: Boolean, default: false },
      },
    ],
    pos,
  },
  files: {
    list: [
      {
        url: { type: String, required: true },
        name: { type: String, required: true },
      },
    ],
    pos,
  },
  allowed: [],
  pos,
  causes: [
    {
      _id: { type: String, required: true },
      description: { type: String, required: true },
      emojis: [
        {
          url: { type: String, required: true },
          pos,
        },
      ],
      images: [
        {
          url: { type: String, required: true },
          pos,
        },
      ],
      comments: {
        list: [],
        pos,
      },
      links: {
        list: [],
        pos,
      },
      votes: {
        list: [],
        pos,
      },
      videos: {
        list: [
          {
            url: { type: String, required: true },
            name: { type: String, required: true },
            upload: { type: Boolean, default: false },
          },
        ],
        pos,
      },
      files: {
        list: [
          {
            url: { type: String, required: true },
            name: { type: String, required: true },
          },
        ],
        pos,
      },
      pos,
      ideas: [
        {
          _id: { type: String, required: true },
          description: { type: String, required: true },
          emojis: [
            {
              url: { type: String, required: true },
              pos,
            },
          ],
          images: [
            {
              url: { type: String, required: true },
              pos,
            },
          ],
          comments: {
            list: [],
            pos,
          },
          links: {
            list: [],
            pos,
          },
          votes: {
            list: [],
            pos,
          },
          videos: {
            list: [
              {
                url: { type: String, required: true },
                name: { type: String, required: true },
                upload: { type: Boolean, default: false },
              },
            ],
            pos,
          },
          files: {
            list: [
              {
                url: { type: String, required: true },
                name: { type: String, required: true },
              },
            ],
            pos,
          },
          pos,
          calculus: {
            pos,
            weights: {
              cost: Number,
              time: Number,
              resources: Number,
            },
            rating: {
              cost: Number,
              time: Number,
              resources: Number,
            },
            total: Number,
          },
        },
      ],
    },
  ],
});

module.exports = mongoose.model('problem', problemSchema);
