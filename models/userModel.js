const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true,
  },
  name: String,
  surname: String,
  avatar: String,
  activation: String,
  recovery: String,
  role: String,
  hash: String,
});

userSchema.methods.setPassword = function (password) {
  this.hash = crypto.pbkdf2Sync(password, process.env.BCF, 1000, 64, 'sha1').toString('hex');
};

userSchema.methods.validPassword = function (password) {
  const hash = crypto.pbkdf2Sync(password, process.env.BCF, 1000, 64, 'sha1').toString('hex');
  return this.hash === hash;
};

userSchema.methods.generateJwt = function () {
  const expiry = new Date();
  expiry.setDate(expiry.getDate() + 365);

  // El rol se "encripta" como un base64 juntando el id de user y el email, después se "desencriptará" en el frontend para matchear con el valor del rol.

  return jwt.sign(
    Object.assign(
      {},
      {
        _id: this._id,
        avatar: this.avatar,
        name: this.name,
        surname: this.surname,
        email: this.email,
        exp: parseInt(expiry.getTime() / 1000),
      },
      this.role && {
        role: Buffer.from(`${this._id}${this.role}${this.email}`).toString('base64'),
      },
    ),
    process.env.BCF,
  );
};
userSchema.methods.generateMailToken = function () {
  const expiry = new Date();
  expiry.setDate(expiry.getDate() + 1);

  const token = jwt.sign(
    {
      uid: this._id,
      exp: parseInt(expiry.getTime()),
    },
    process.env.BCF,
  );

  this.recovery = token;

  return token;
};

userSchema.methods.generateActivationToken = function () {
  const expiry = new Date();
  expiry.setDate(expiry.getDate() + 1);

  const token = jwt.sign(
    {
      acuid: this._id,
      exp: parseInt(expiry.getTime()),
    },
    process.env.BCF,
  );

  this.activation = token;

  return token;
};

module.exports = mongoose.model('user', userSchema);
