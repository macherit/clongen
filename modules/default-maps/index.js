const fs = require("fs");
const path = require("path");
const problemModel = require("../../models/problemModel");

const maps = [];

// maps.push(JSON.parse(fs.readFileSync(path.join(__dirname, './Navigation v1.json'))));
maps.push(
  JSON.parse(fs.readFileSync(path.join(__dirname, "./Navigation v2.json")))
);
// maps.push(JSON.parse(fs.readFileSync(path.join(__dirname, './Chicago Crime v1.json'))));
maps.push(
  JSON.parse(fs.readFileSync(path.join(__dirname, "./Chicago Crime v1 d.json")))
);
// maps.push(
//   JSON.parse(fs.readFileSync(path.join(__dirname, './New Educational Curriculum v1.json'))),
// );
maps.push(
  JSON.parse(
    fs.readFileSync(path.join(__dirname, "./Features of IdeaGens.json"))
  )
);

const s4 = () =>
  Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);

const guid = () => `${s4()}-${s4()}-${s4()}`;

const fixMap = (map, uid) => {
  const newMap = {
    _id: guid(),
    ...map,
    owner: uid,
    createdAt: new Date(),
    updatedAt: new Date(),
    actions: {
      [uid]: {
        favourite: false,
        trash: false
      }
    }
  };
  newMap.causes = newMap.causes.map(cause => {
    cause = {
      _id: guid(),
      ...cause
    };
    cause.ideas = cause.ideas.map(idea => {
      idea = {
        _id: guid(),
        ...idea
      };
      return idea;
    });
    return cause;
  });
  return newMap;
};

module.exports = uid => {
  maps.map(m => {
    const map = fixMap(m, uid);
    const problem = new problemModel(map);
    problem.save((err, problem) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Problem crated corrcetly");
      }
    });
  });
};
