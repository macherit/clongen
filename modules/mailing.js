"use strict";
var express = require("express");
var router = express.Router();
const nodemailer = require("nodemailer");

let globalTransporter = {
  service: "gmail",
  auth: {
    user: "no-reply@ideagens.com",
    pass: "dear123a"
  },
  secure: true
};

router.post("/contact", (req, res) => {
  const template = require("./templates/contact");
  if (req.body.email)
    nodemailer.createTestAccount((err, account) => {
      let transporter = nodemailer.createTransport(globalTransporter);

      let noErrors = true;

      transporter.sendMail(
        {
          from: process.env.MAIL_ACCOUNT,
          to: req.body.email,
          subject: "Contact from Ideagens",
          text: template.user.text(req),
          html: template.user.html(req)
        },
        (error, info) => {
          if (error) noErrors = false;
        }
      );
      if (noErrors)
        transporter.sendMail(
          {
            from: process.env.MAIL_ACCOUNT,
            to: "avidear@ideagens.com",
            subject: "Contact from Ideagens",
            text: template.client.text(req),
            html: template.client.html(req)
          },
          (error, info) => {
            console.log(error);
            if (error) res.status(500).json({ message: "Ocurrió un error" });
            else res.json();
          }
        );
      else return res.status(500).json({ message: "Ocurrió un error" });
    });
  else return res.status(500).json({ message: "Datos incompletos" });
});

module.exports = router;
