const express = require('express');
const nodemailer = require('nodemailer');

const globalTransporter = {
  service: "gmail",
  auth: {
    user: "no-reply@ideagens.com",
    pass: "dear123a"
  },
  secure: true
};

module.exports = (email, token, callback) => {
  const template = require('./templates/mca');
  nodemailer.createTestAccount((err, account) => {
    const transporter = nodemailer.createTransport(globalTransporter);

    const noErrors = true;

    transporter.sendMail(
      {
        from: process.env.MAIL_ACCOUNT,
        to: email,
        subject: 'Contact from ideagens',
        text: template.user.text(token),
        html: template.user.html(token),
      },
      callback,
    );
  });
};
