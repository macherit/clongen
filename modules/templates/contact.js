module.exports = {
  user: {
    text: req => "We've received your request, we'll contact you soon.\nSincerely. Ideagens.",
    html: req => "We've received your request, we'll contact you soon.<br />Sincerely. Ideagens."
  },
  client: {
    text: req => `The following request was submitted through the contact form:\nName: ${req.body.name ? req.body.name : '--'}\nEmail: ${req.body.email ? req.body.email : '--'}\nSubject: ${req.body.subject ? req.body.subject : '--'}\nComment: ${req.body.comment ? req.body.comment : '--'}`,
    html: req => `The following request was submitted through the contact form:<br />Name: ${req.body.name ? req.body.name : '--'}<br />Email: ${req.body.email ? req.body.email : '--'}<br />Subject: ${req.body.subject ? req.body.subject : '--'}<br />Comment: <p style="white-space: pre-line;">${req.body.comment ? req.body.comment : '--'}</p>`
  }
};
