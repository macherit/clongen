## How to run IdeaGens

### Requirements

- NodeJS (Server framework).
- npm (NodeJS package management tool).
- pm2 (Tool for clustering NodeJS applications)
- MongoDB (DataBase; you can run it in the same machine as the rest or you can use a service like 'mLab' o 'DigitalOcean').
- imagemagick (Required to be able to export as image an PDF).
- graphicsmagick (Required to be able to export as image an PDF).
- Apache (To serve the static files).

### NodeJS Server

1. Open a terminal inside the project's folder.
2. Run 'npm install'.
3. Edit the '.env' file located in the root of the project.
    - The file contains the declaration of the variables that need to be defined to be able to run the server.
    - Each variable is written as 'KEY="VALUE"'.
    - Read the comment precedeing each variable and be sure to define them all, otherwise the server won't run or it will run incorrectly.
4. Run chmod +x 'exec-server'.

### Apache

1. Edit the 'apache.conf' file located in the root of the project changing all occurrences of <XXXXX> so that it fits your server configuration.
2. Move that file inside your apache configuration to serve all the static resources.
- All static files related to the frontend are in the 'frontend' folder

### Running

- Apache should already be running to be able to see the frontend application.
- Edit the 'exec-server' file located in the root of the project changing all occurrences of <XXXXX> so that it fits your server configuration and uncomment the last two lines.
- Run './exec-server' everytime you want to start or restart the server.
