const { convert } = require('convert-svg-to-png');
const fs = require('fs');
const gm = require('gm').subClass({ imageMagick: true });

module.exports = {
  convert: async (req, res) => {
    const sizes = Array.isArray(req.query.sizes) ? req.query.sizes : [req.query.sizes];
    const png = await convert(req.body.svgString, { puppeteer: { args: ['--no-sandbox'] } });
    const outImages = [];
    const proms = [];
    sizes.map((s, i) => {
      const size = JSON.parse(s);
      proms.push(
        new Promise((resolve, reject) => {
          gm(png)
            .extent(
              size.width,
              size.height,
              `${size.x >= 0 ? '+' : ''}${size.x}${size.y >= 0 ? '+' : ''}${size.y}`,
            )
            .toBuffer('PNG', (error, buffer) => {
              if (error) {
                console.log(error);
                reject();
              } else {
                outImages[i] = buffer.toString('base64');
                resolve();
              }
            });
        }),
      );
    });
    Promise.all(proms).then(
      () => {
        res.json(outImages);
      },
      error => {
        res.staus(500).json({ message: 'Error' });
      },
    );
  },
};
