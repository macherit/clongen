const passport = require('passport');
const userModel = require('../models/userModel.js');
const mongoose = require('mongoose');
const mpc = require('../modules/mpc.js');
const mca = require('../modules/mca.js');

/**
 * userController.js
 *
 * @description :: Server-side logic for managing users.
 */
module.exports = {
  /**
   * userController.list()
   */
  list(req, res) {
    userModel.find({}).exec((err, users) => {
      if (err) {
        return res.status(500).json({
          message: 'Error when getting user.',
          error: err,
        });
      }
      return res.json(users);
    });
  },

  user_exists(req, res) {
    userModel.findOne({ _id: req.payload.uid }).exec((err, usuario) => {
      if (err) {
        return res.status(500).json({});
      }
      if (!usuario) {
        return res.status(404).json({});
      }
      return res.json();
    });
  },

  /**
   * userController.show()
   */
  show(req, res) {
    const id = req.params.id ? req.params.id : req.payload._id;
    userModel.findOne({ _id: mongoose.mongo.ObjectId(id) }, (err, user) => {
      if (err) {
        return res.status(500).json({
          message: 'Error when getting user.',
          error: err,
        });
      }
      if (!user) {
        return res.status(404).json({
          message: 'No such user',
        });
      }
      return res.json(user);
    });
  },
  // //////////////
  mail_pass_check: (req, res) => {
    if (req.body.email) {
      userModel.findOne({ email: req.body.email }, (err, user) => {
        if (err) {
          return res.status(500).json({
            message: 'Error when getting user.',
            error: err,
          });
        }
        if (user) {
          user.generateMailToken();
          user.save((err, user) => {
            if (err) {
              return res.status(500).json({
                message: 'Error when sending email',
                error: err,
              });
            }
            mpc(user.email, user.recovery, (error, info) => {
              if (error) {
                return res.status(500).json({ message: 'An error occurred' });
              }
              return res.json();
            });
          });
        } else {
          return res.status(404).json();
        }
      });
    } else {
      return res.status(400).json({
        message: 'Error',
      });
    }
  },
  // //////////////
  login(req, res) {
    passport.authenticate('local', (err, user, info) => {
      let token;

      // If Passport throws/catches an error
      if (err) {
        res.status(404).json(err);
        return;
      }

      // If a user is found
      if (user) {
        if (!user.activation) {
          token = user.generateJwt();
          res.status(200);
          res.json({ token });
        } else {
          res.status(409).json({ message: 'Requires activation' });
        }
      } else {
        // If user is not found
        res.status(401).json(info);
      }
    })(req, res);
  },

  /**
   * userController.create()
   */
  create(req, res) {
    const user = new userModel({
      email: req.body.email,
      hash: req.body.hash,
      role: req.body.role,
    });

    user.setPassword(req.body.password);
    user.generateActivationToken();

    user.save((err, user) => {
      if (err) {
        return res.status(500).json({
          message: 'Error when creating user',
          error: err,
        });
      }
      let token;
      token = user.generateJwt();

      mca(user.email, user.activation, (error, info) => {
        if (error) {
          console.log(error);
          return res.status(500).json({ message: 'An error occurred' });
        }
        return res.status(201).json();
      });
    });
  },

  account_activation(req, res) {
    if (req.payload.acuid) {
      userModel.findOne({ _id: req.payload.acuid }, (err, user) => {
        if (err) {
          return res.status(500).json({
            message: 'Error when getting user',
            error: err,
          });
        } else if (!user) {
          return res.status(404).json({
            message: 'No such user',
          });
        }
        user.activation = '';

        user.save((err, user) => {
          if (err) {
            return res.status(500).json({
              message: 'Error when updating user.',
              error: err,
            });
          }
          const token = user.generateJwt();
          return res.json({ tk: token });
        });
      });
    } else {
      return res.status(400).json({
        message: 'Error',
      });
    }
  },

  /**
   * userController.update()
   */
  update(req, res) {
    const id = req.payload._id;
    userModel.findOne({ _id: mongoose.mongo.ObjectId(id) }, (err, user) => {
      if (err) {
        return res.status(500).json({
          message: 'Error when getting user',
          error: err,
        });
      }
      if (!user) {
        return res.status(404).json({
          message: 'No such user',
        });
      }

      user.avatar = req.body.avatar ? req.body.avatar : user.avatar;
      user.name = req.body.name ? req.body.name : user.name;
      user.surname = req.body.surname ? req.body.surname : user.surname;

      user.save((err, user) => {
        if (err) {
          return res.status(500).json({
            message: 'Error when updating user.',
            error: err,
          });
        }

        return res.json({
          email: user.email,
          _id: user._id,
          avatar: user.avatar,
          tk: user.generateJwt(),
        });
      });
    });
  },
  // /////////////
  ch_pass(req, res) {
    if (req.body.password) {
      userModel.findOne({ _id: req.payload.uid }, (err, user) => {
        if (err) {
          return res.status(500).json({
            message: 'Error when getting user',
            error: err,
          });
        } else if (!user) {
          return res.status(404).json({
            message: 'No such user',
          });
        }

        user.recovery = '';
        user.setPassword(req.body.password);

        user.save((err, user) => {
          if (err) {
            return res.status(500).json({
              message: 'Error when updating user.',
              error: err,
            });
          }
          return res.json(user);
        });
      });
    } else {
      return res.status(400).json({
        message: 'Error',
      });
    }
  },
  // /////////
  /**

     * userController.remove()
  remove: function(req, res) {
     */
  remove(req, res) {
    const id = req.payload._id;
    userModel.findByIdAndRemove(mongoose.mongo.ObjectId(id), (err, user) => {
      if (err) {
        return res.status(500).json({
          message: 'Error when deleting the user.',
          error: err,
        });
      }
      return res.status(204).json();
    });
  },
};
