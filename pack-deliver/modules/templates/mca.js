module.exports = {
  user: {
    text: token =>
      `You've requested an account creation, follow this link to finish the sign up:\nhttp://ideagens.com/u/signup-confirm?tk=${token}`,
    html: token =>
      `You've requested an account creation, follow this link to finish the sign up:<br /><a href='http://ideagens.com/u/signup-confirm?tk=${token}' style='padding: 10px; background-color: rgb(10, 163, 238); color: #fff; text-decoration: none;border-radius: 5px;margin-top: 10px;margin-bottom: 10px;display:block;width: 100px; text-align: center;'>Continue</a><span style='font-size: 13px;font-style: italic;display:block;'>If you cannot see the link correctly, click here: http://ideagens.com/u/signup-confirm?tk=${token}</span>`,
  },
};
