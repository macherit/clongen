const express = require('express');

const router = express.Router();
const problemController = require('../controllers/problemController.js');
const guardian = require('../config/guardian');

/*
 * GET
 */
router.get('/my', guardian({}), problemController.my);
router.get('/', guardian({}), problemController.list);

/*
 * GET
 */
router.get('/:id/is_public', problemController.is_public);

router.get('/:id/get_public', problemController.get_public);

router.get('/:id', guardian({}), problemController.show);

/*
 * POST
 */
router.post('/', guardian({}), problemController.create);

/*
 * PUT
 */
router.put('/allow/:id', guardian({}), problemController.allow);
router.put('/disallow/:id', guardian({}), problemController.disallow);
router.put('/:id', guardian({}), problemController.update);

router.put('/:id/favourite', guardian({}), problemController.favourite);
router.put('/:id/unfavourite', guardian({}), problemController.unfavourite);

router.put('/:id/public', guardian({}), problemController.public);
router.put('/:id/unpublic', guardian({}), problemController.unpublic);

router.put('/:id/trash', guardian({}), problemController.trash);
router.put('/:id/untrash', guardian({}), problemController.untrash);

/*
 * DELETE
 */
router.delete('/:id', guardian({}), problemController.remove);

module.exports = router;
