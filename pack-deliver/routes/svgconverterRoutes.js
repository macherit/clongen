const express = require('express');
const svgconverterController = require('../controllers/svgconverterController');

const router = express.Router();

router.post('/', svgconverterController.convert);

module.exports = router;
