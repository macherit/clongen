const express = require('express');

const router = express.Router();
const userController = require('../controllers/userController');
const guardian = require('../config/guardian');

/*
 * GET
 */
// router.get('/', userController.list);
router.get('/', guardian({}), userController.list);

/*
 * GET
 */
router.put(
  '/account_activation',
  guardian({ activationToken: true }),
  userController.account_activation,
);

router.get('/uex', guardian({ mailToken: true }), userController.user_exists);

router.get('/me', guardian({}), userController.show);
router.get('/:id', guardian({}), userController.show);

/*
 * POST
 */
router.post('/mail_pass_check', userController.mail_pass_check);
/*
  Verifica que exista un user con el mail indicado, si existe, envía un email directamente al email del user con un token especial (especial dado que no sirve para realizar un login) para realizar la recuperación
*/
router.post('/login', userController.login);
router.post('/', userController.create);

/*
 * PUT
 */

router.put('/ch_pass', guardian({ mailToken: true }), userController.ch_pass);
router.put('/:id', guardian({}), userController.update);
/*
  El guardian valida la existencia de un user con _id igual al uid del token especial, de ser así pasa la petición al controlador, este, setea la nueva contraseña de user
*/

/*
 * DELETE
 */
router.delete('/:id', guardian({}), userController.remove);

module.exports = router;
