export default {
  install(Vue, options) {
    Vue.prototype.adjustPrinter = (options = false) => {
      document.querySelectorAll('.printer').forEach((printer, i) => {
        if (printer) {
          const dgSvg = document.querySelector('#diagram svg > g');
          if (dgSvg) {
            const prbx = dgSvg.getBoundingClientRect();
            printer.style.top = `${window.Graph.view.graphBounds.y}px`;
            printer.style.left = `${window.Graph.view.graphBounds.x}px`;
            if (options.shouldResize) {
              printer.style.width = `${window.Graph.view.graphBounds.width}px`;
              printer.style.height = `${window.Graph.view.graphBounds.height}px`;
              if (options.width && options.height) {
                printer.style.width = `${options.width}px`;
                printer.style.height = `${options.height}px`;
              }
              if (options.translations && options.translations[i]) {
                printer.style.webkitTransform = printer.style.transform = options.translations[i];
                printer.style.margin = '0px';
              }
            }
          }
        }
      });
    };
  },
};
