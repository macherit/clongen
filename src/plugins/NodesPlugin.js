export default {
  install(Vue, options) {
    const guid = () => {
      const s4 = () =>
        Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);

      return `${s4()}-${s4()}-${s4()}`;
    };
    Vue.prototype.guid = guid;

    function copyPos() {
      return {
        x: 0,
        y: 0,
        w: 0,
        h: 0,
      };
    }

    Vue.prototype.copyPos = copyPos;

    function calculus() {
      return {
        pos: copyPos(),
        weights: {
          cost: 0,
          time: 0,
          resources: 0,
        },
        rating: {
          cost: 0,
          time: 0,
          resources: 0,
        },
        total: 0,
      };
    }

    Vue.prototype.calculus = calculus;

    Vue.prototype.baseNode = function () {
      return {
        _id: guid(),
        description: '',
        emojis: [],
        images: [],
        comments: { list: [] },
        links: { list: [] },
        videos: { list: [] },
        files: { list: [] },
        pos: copyPos(),
      };
    };
  },
};
