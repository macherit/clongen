import Vue from 'vue';
import Router from 'vue-router';
import axios from 'axios';

import Features from '@/views/Features/Index';
import Pricing from '@/views/Pricing/Index';
import Tutorial from '@/views/Tutorial/Index';
import Contact from '@/views/Contact/Index';
import Logout from '@/views/User/Logout';
import HomeIndex from '@/views/Home/Index';
import Signup from '@/views/User/Signup';
import Recovery from '@/views/User/Recovery';
import ConfirmSignup from '@/views/User/ConfirmSignup';
import Pwdchg from '@/views/User/Pwdchg';
import Profile from '@/views/User/Profile';
import ProblemIndex from '@/views/Problem/Index';
import ProblemNewEdit from '@/views/Problem/NewEdit';

import store from '@/store/index';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HomeIndex,
    },
    {
      path: '/features',
      name: 'Features',
      component: Features,
    },{
      path: '/pricing',
      name: 'Pricing',
      component: Pricing,
    },{
      path: '/tutorial',
      name: 'Tutorial',
      component: Tutorial,
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact,
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Logout,
      meta: {
        requiresLogin: true,
      },
    },
    {
      path: '/u',
      name: 'Profile',
      component: Profile,
      meta: {
        requiresLogin: true,
      },
    },
    {
      path: '/u/signup',
      name: 'Signup',
      component: Signup,
      meta: {
        requiresLoggedOut: true,
      },
    },
    {
      path: '/u/signup-confirm',
      name: 'ConfirmSignup',
      component: ConfirmSignup,
      meta: {
        requiresLoggedOut: true,
      },
    },
    {
      path: '/u/password-recovery',
      name: 'Recovery',
      component: Recovery,
      meta: {
        requiresLoggedOut: true,
      },
    },
    {
      path: '/u/pwdchg',
      name: 'Pwdchg',
      component: Pwdchg,
      meta: {
        requiresLoggedOut: true,
      },
    },
    {
      path: '/problems',
      name: 'Problem#Index',
      component: ProblemIndex,
      meta: {
        requiresLogin: true,
      },
    },
    {
      path: '/p',
      name: 'Problem#New',
      component: ProblemNewEdit,
      children: [
        {
          path: '/p/:pid',
          name: 'Problem#Edit',
          component: ProblemNewEdit,
          meta: {
            requiresLoginOrPublic: true,
          },
        },
      ],
      meta: {
        requiresLoginOrPublic: true,
      },
    },
    { path: '*', redirect: '/' },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresLogin)) {
    if (!store.state.users.loggedIn) {
      next({
        path: '/',
      });
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.requiresLoggedOut)) {
    if (store.state.users.loggedIn) {
      next({
        path: '/problems',
      });
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.requiresLoginOrPublic)) {
    if (store.state.users.loggedIn) {
      next();
    } else if (to.params.pid) {
      axios({
        method: 'GET',
        url: `/api/problems/${to.params.pid}/is_public`,
      }).then(
        ({ status }) => {
          if (status === 200) {
            next();
          } else {
            next({
              path: '/',
            });
          }
        },
        error => {
          next({
            path: '/',
          });
        },
      );
    } else {
      next({
        path: '/',
      });
    }
  } else {
    next();
  }
});

export default router;
