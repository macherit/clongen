export default {
  login(email, password, callback) {
    return window.App.$http({
      method: 'POST',
      url: '/api/users/login',
      body: { email, password },
    }).then(
      res => {
        if (res.status === 200) {
          localStorage.setItem('@clonGen:user', res.data.token);
        }
        callback(null, res);
      },
      error => {
        callback(error, null);
      },
    );
  },
  resetTK(tk) {
    localStorage.setItem('@clonGen:user', tk);
  },
  logout() {
    localStorage.removeItem('@clonGen:user');
  },
};
