export default {
  namespaced: true,
  state: {
    popup: {
      visible: false,
      active: '',
      width: false,
    },
    printer: {
      visible: false,
      draggable: true,
      count: 0,
      scaleFactor: 1,
      paperSize: '',
      printingFactor: 1,
    },
    immersiveMode: false,
  },
  getters: {
    isPopupVisible: state => state.popup.visible,
    isPopupActive: state => name => state.popup.active === name,
  },
  actions: {
    showPopup({ commit }, { name, width = false }) {
      commit('showPopup', { name, width });
    },
    hidePopup({ commit }) {
      commit('hidePopup');
    },
    showPrinter({ commit }) {
      commit('showPrinter');
    },
    setPrinterDrageble({ commit }) {
      commit('setPrinterDrageble');
    },
    setPrinterFixed({ commit }) {
      commit('setPrinterFixed');
    },
    hidePrinter({ commit }) {
      commit('hidePrinter');
    },
    increasePrinters({ commit }) {
      commit('increasePrinters');
    },
    decreasePrinters({ commit }) {
      commit('decreasePrinters');
    },
    setScaleFactor({ commit }, factor) {
      commit('setScaleFactor', factor);
    },
    setPrintingFactor({ commit }, factor) {
      commit('setPrintingFactor', factor);
    },
    setPaperSize({ commit }, paperSize) {
      commit('setPaperSize', paperSize);
    },
    toggleImmersiveMode({ commit }) {
      commit('toggleImmersiveMode');
    },
  },
  mutations: {
    showPopup(state, { name, width }) {
      console.log(`popup ${name} visible`);
      state.popup.visible = true;
      state.popup.active = name;
      state.popup.width = width;
    },
    hidePopup(state) {
      console.log('popup hidden');
      state.popup.visible = false;
      state.popup.active = '';
    },
    showPrinter(state) {
      console.log('printer visible');
      state.printer.visible = true;
    },
    setPrinterDrageble(state) {
      console.log('printer draggable');
      state.printer.draggable = true;
    },
    setPrinterFixed(state) {
      console.log('printer fixed');
      state.printer.draggable = false;
    },
    hidePrinter(state) {
      console.log('printer hidden');
      state.printer.visible = false;
      state.printer.draggable = true;
    },
    increasePrinters(state) {
      state.printer.count += 1;
    },
    decreasePrinters(state) {
      if (state.printer.count > 0) {
        state.printer.count -= 1;
      }
    },
    setScaleFactor(state, factor) {
      state.printer.scaleFactor = factor;
    },
    setPrintingFactor(state, factor) {
      state.printer.printingFactor = factor;
    },
    setPaperSize(state, paperSize) {
      state.printer.paperSize = paperSize;
    },
    toggleImmersiveMode(state) {
      if (state.immersiveMode) {
        state.immersiveMode = false;
      } else {
        state.immersiveMode = true;
      }
    },
  },
};
