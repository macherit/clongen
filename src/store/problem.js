import { iqIdeas, maxIdeas, iqCauses, maxCauses } from '@/constants';

const s4 = () =>
  Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);

const guid = () => `${s4()}-${s4()}-${s4()}`;

const copyPos = () => ({
  x: 0,
  y: 0,
  w: 0,
  h: 0,
});

const calculus = () => ({
  pos: copyPos(),
  weights: {
    cost: 0,
    time: 0,
    resources: 0,
  },
  rating: {
    cost: 0,
    time: 0,
    resources: 0,
  },
  total: 0,
});

const baseNode = () => ({
  _id: guid(),
  description: '',
  emojis: [],
  images: [],
  comments: { list: [] },
  links: { list: [] },
  votes: { list: [] },
  videos: { list: [] },
  files: { list: [] },
  pos: copyPos(),
});

const initialProblem = (qCauses, qIdeas, user) => {
  const problem = {
    ...baseNode(),
    public: false,
    actions: {},
    weights: {
      cost: 0,
      time: 0,
      resources: 0,
    },
    causes: [],
    allowed: [],
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  problem.actions[user._id] = {
    favourite: false,
    trash: false,
  };
  for (let i = 0; i < qCauses; i += 1) {
    problem.causes.push({
      ...baseNode(),
      ideas: [],
    });
    for (let j = 0; j < qIdeas; j += 1) {
      problem.causes[i].ideas.push({
        ...baseNode(),
        calculus: calculus(),
      });
    }
  }
  return problem;
};

const addNodesToMap = (problem, objectMap) => {
  const addId = node => {
    if (!node._id) {
      node._id = guid();
    }
    objectMap.set(`${node._id}`, node);
  };
  addId(problem);
  for (let i = 0; i < problem.causes.length; i += 1) {
    addId(problem.causes[i]);
    for (let j = 0; j < problem.causes[i].ideas.length; j += 1) {
      addId(problem.causes[i].ideas[j]);
    }
  }
};

const fixMissingNodes = (problem, qCauses, qIdeas) => {
  const outProblem = Object.assign(
    {},
    {
      ...baseNode(),
      weights: {
        cost: 0,
        time: 0,
        resources: 0,
      },
      causes: [],
      allowed: [],
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    problem,
  );
  const maxCauses = Math.max(outProblem.causes.length, qCauses);
  let maxIdeas = qIdeas;
  outProblem.causes.map(c => {
    maxIdeas = Math.max(c.ideas.length, maxIdeas);
  });
  for (let i = 0; i < maxCauses; i += 1) {
    if (!outProblem.causes[i]) {
      outProblem.causes.push({
        ...baseNode(),
        ideas: [],
      });
    } else {
      outProblem.causes[i] = Object.assign(
        {},
        {
          ...baseNode(),
          ideas: [],
        },
        outProblem.causes[i],
      );
    }
    for (let j = 0; j < maxIdeas; j += 1) {
      if (!outProblem.causes[i].ideas[j]) {
        outProblem.causes[i].ideas.push({
          ...baseNode(),
          calculus: calculus(),
        });
      } else {
        outProblem.causes[i].ideas[j] = Object.assign(
          {},
          {
            ...baseNode(),
            calculus: calculus(),
          },
          outProblem.causes[i].ideas[j],
        );
      }
    }
  }
  return outProblem;
};

export default {
  namespaced: true,
  state: {
    problem: {},
    qCauses: iqCauses,
    qIdeas: iqIdeas,
    objectMap: new Map(),
    selectedNode: false,
    canEdit: true,
  },
  getters: {
    getNodeById: state => id => state.objectMap.get(id),
    canEdit: state => user =>
      (state.problem.owner && state.problem.allowed
        ? state.problem.owner === user._id ||
          state.problem.allowed.find(a => a.email === user.email && a.permission === 'edit')
        : true),
  },
  actions: {
    restartProblem({ commit, rootState }) {
      let user = {};
      if (rootState.users.token) {
        user = JSON.parse(window.atob(rootState.users.token.split('.')[1]));
      }
      commit('restartProblem', user);
    },
    setProblem({ commit, rootState }, problem) {
      let user = {};
      if (rootState.users.token) {
        user = JSON.parse(window.atob(rootState.users.token.split('.')[1]));
      }
      commit('setProblem', { problem, user });
    },
    toggleFavourite({ commit }) {
      commit('toggleFavourite');
    },
    toggleTrash({ commit }) {
      commit('toggleTrash');
    },
    setSelectedNode({ commit }, node) {
      commit('setSelectedNode', node);
    },
    addCause({ commit }, index) {
      commit('addCause', index);
    },
    removeCause({ commit }, index) {
      commit('removeCause', index);
    },
    addIdeas({ commit }, index) {
      commit('addIdeas', index);
    },
    removeIdeas({ commit }, index) {
      commit('removeIdeas', index);
    },
    setLoadedProblem({ commit }) {
      commit('setLoadedProblem');
    },
  },
  mutations: {
    restartProblem(state, user) {
      console.log('problem restarted');
      const problem = initialProblem(state.qCauses, state.qIdeas, user);
      const objectMap = new Map();
      addNodesToMap(problem, objectMap);
      state.problem = problem;
      state.objectMap = objectMap;
      state.canEdit = true;
    },
    setProblem(state, { problem, user }) {
      console.log('problem setted');
      const fixedProblem = fixMissingNodes(problem, state.qCauses, state.qIdeas);
      const objectMap = new Map();
      addNodesToMap(fixedProblem, objectMap);
      if (!fixedProblem.actions) {
        fixedProblem.actions = {};
      }
      if (!fixedProblem.actions[user._id]) {
        fixedProblem.actions[user._id] = {
          favourite: false,
          trash: false,
        };
      }
      state.problem = fixedProblem;
      state.objectMap = objectMap;
      state.canEdit =
        state.problem.owner && state.problem.allowed
          ? state.problem.owner === user._id ||
            state.problem.allowed.find(a => a.email === user.email && a.permission === 'edit')
          : true;
    },
    toggleFavourite(state) {
      console.log('problem un/favourited');
      if (state.problem.favourite) {
        state.problem.favourite = false;
      } else {
        state.problem.favourite = true;
      }
    },
    toggleTrash(state) {
      console.log('problem un/trashed');
      if (state.problem.trash) {
        state.problem.trash = false;
      } else {
        state.problem.trash = true;
      }
    },
    setSelectedNode(state, node) {
      console.log('node selected');
      state.selectedNode = node;
    },
    addCause(state, index) {
      const cause = {
        ...baseNode(),
        ideas: [],
      };
      state.problem.causes.splice(index + 1, 0, cause);
      for (let j = 0; j < state.problem.causes[0].ideas.length; j += 1) {
        cause.ideas.push({
          ...baseNode(),
          calculus: calculus(),
        });
      }
      console.log('added cause row');
    },
    removeCause(state, index) {
      if (state.problem.causes.length > 1) {
        state.problem.causes.splice(index, 1);
        console.log('removed cause row');
      }
    },
    addIdeas(state, index) {
      state.problem.causes.map(cause => {
        cause.ideas.splice(index, 0, {
          ...baseNode(),
          calculus: calculus(),
        });
      });
      console.log('added ideas row');
    },
    removeIdeas(state, index) {
      if (state.problem.causes[0].ideas.length > 1) {
        state.problem.causes.map(cause => {
          cause.ideas.splice(index - 1, 1);
        });
        console.log('removed ideas row');
      }
    },
    setLoadedProblem(state) {
      state.problem.wasLoaded = true;
      console.log('problem marked as loaded');
    },
  },
};
