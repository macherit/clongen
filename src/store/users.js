import userService from '@/services/user';

const hasTokenExpired = token => {
  const outObj = { loggedIn: true, token };

  if (new Date(JSON.parse(window.atob(token.split('.')[1])).exp * 1000) < new Date()) {
    // Token outdated
    outObj.loggedIn = false;
    outObj.token = null;

    userService.logout();
  }

  return outObj;
};

export default {
  namespaced: true,
  state: localStorage.getItem('@clonGen:user')
    ? hasTokenExpired(localStorage.getItem('@clonGen:user'))
    : { loggedIn: false, token: null },
  getters: {
    user(state) {
      let outObj = false;
      if (state.loggedIn) {
        const user = JSON.parse(window.atob(state.token.split('.')[1]));
        outObj = {
          _id: user._id,
          email: user.email,
          avatar: user.avatar,
          name: user.name,
          surname: user.surname,
        };
      }
      return outObj;
    },
  },
  actions: {
    login({ commit }, { email, password, callback }) {
      userService.login(email, password, (error, res) => {
        if (error) {
          callback(error, null);
        } else {
          commit('loginSuccess', res.data.token);
          callback(null, res);
        }
      });
    },
    logout({ commit }) {
      userService.logout();
      commit('logout');
    },
  },
  mutations: {
    loginSuccess(state, token) {
      console.log('login success');
      state.loggedIn = true;
      state.token = token;
    },
    loginFailure(state) {
      console.log('login failure');
      state.loggedIn = false;
      state.token = null;
    },
    logout(state) {
      console.log('logout success');
      state.loggedIn = false;
      state.token = null;
    },
  },
};
