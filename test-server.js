const express = require('express');
const request = require('request');
const path = require('path');
const app = express();

app.use('/api', (req, res) => {
  req
    .pipe(
      request({ url: `http://localhost:40000/api${req.url}` }, (err, res, body) => {
        if (err) {
          console.error(err);
          return;
        }
      })
    )
    .pipe(res);
});

app.use('/public', (req, res) => {
  req
    .pipe(
      request({ url: `http://localhost:40000/public${req.url}` }, (err, res, body) => {
        if (err) {
          console.error(err);
          return;
        }
      })
    )
    .pipe(res);
});

app.use(express.static('dist/'));

app.get('/*', function(req, res) {
  if (req.xhr) {
    return res.status(404).send(req.url + ' not found');
  }
  res.sendfile(path.resolve(__dirname + '/dist/index.html'));
});

app.listen(41000, () => {});
